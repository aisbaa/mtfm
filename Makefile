VIRTUAL = . ./.tox/py35/bin/activate &&

setup:
	tox
	bower install
.PHONY : setup

run:
	$(VIRTUAL) ./manage.py runserver
.PHONY : run
