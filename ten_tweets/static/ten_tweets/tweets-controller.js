angular
    .module("ten-tweets", [])
    .controller(
        "TweetListController", function($scope, $http) {
            $scope.tweets = [];

            $scope.updateMap = function() {
                _.each($scope.tweets, function(tweet, index) {
                    if (!tweet.country) {
                        console.log('skip tweet, no country')
                        return;
                    }

                    var coordinates = tweet.country.coordinates;
                    var modified = [
                        coordinates[0] + (Math.random() - 0.5) * 0.1,
                        coordinates[1] + (Math.random() - 0.5) * 0.1
                    ];

                    console.log(tweet.text);
                    console.log(coordinates);
                    console.log(modified);
                    L.popup().setLatLng(
                        modified
                    ).setContent(
                        tweet.text
                    ).openOn(
                        window.map
                    );
                });
            };

            // main

            $http.get("/10").then(function(response) {
                $scope.tweets = response.data;
                $scope.updateMap()
            });

        }
    );
