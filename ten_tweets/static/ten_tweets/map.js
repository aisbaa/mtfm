(function() {
    var map = L.map('map').setView([24, 8.5], 1.49);

    L.tileLayer(
        'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        {
            attribution: '<a href="http://openstreetmap.org">OpenStreetMap</a>',
            maxZoom: 18,
            id: '',
        }
    ).addTo(map);

    window.map = map;
})();
