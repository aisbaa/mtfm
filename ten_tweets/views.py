import csv
import os.path
import re
import twitter
import six

from django.conf import settings
from django.http import JsonResponse
from django.views.generic.base import TemplateView, View


class IndexView(TemplateView):
    template_name = 'index.html'


def create_coordinate_mapping(filename):
    """Retuns three dicts of country location data

    1) country name or code to coordinates
    2) country name to country code
    3) country code to country name
    """
    if six.PY2:
        csv_file = open(filename, 'rb')
    elif six.PY3:
        csv_file = open(filename, encoding='UTF-8')
    else:
        raise RuntimeError('not supported python version')

    coordiate_mapping = {}
    name_to_code = {}
    code_to_name = {}

    for country in csv.DictReader(csv_file):
        coordinates = (
            float(country['lat']) if country['lat'] != 'None' else None,
            float(country['lng']) if country['lng'] != 'None' else None,
        )
        coordiate_mapping[country['name']] = coordinates
        coordiate_mapping[country['code']] = coordinates

        name_to_code[country['name']] = country['code']
        code_to_name[country['code']] = country['name']

    csv_file.close()
    return (
        coordiate_mapping,
        name_to_code,
        code_to_name
    )


class Latest10Tweets(View):

    COORDINATE_FILENAME = os.path.join(
        os.path.dirname(__file__),
        'countries.csv'
    )

    (
        COORDINATES,
        COUNTRY_NAME_TO_CODE,
        COUNTRY_CODE_TO_NAME
    ) = create_coordinate_mapping(COORDINATE_FILENAME)

    # re patter: 'GB|United Kingdom|DE|Germany|....
    COUNTRY_NAME_RE = re.compile('[\@\# ](?P<country>{})\\b'.format('|'.join(
        re.escape(key) for key in COORDINATES.keys())))

    @property
    def twitter_api(self):
        # TODO(Aistis): move credentials to settings
        # TODO(Aistis): change credentials
        # TODO(Aistis): find a way not to commit credentials
        return twitter.Api(
            **settings.TWITTER_ACCOUNT_CREDENTIALS
        )

    def detect_country_text_parse(self, tweet):
        """Tries to detect country form text

        Args:
            tweet (dict): one tweet returned by API.

        Example:

            >>> latest_10_tweets = Latest10Tweets()
            >>> tweet = {
            ...     'text': ('The official account of the Eurovision Song '
            ...              'Contest. The 2017 Contest will take place on '
            ...              '9, 11 and 13 May in Kyiv, Ukraine!),
            ... }
            >>> latest_10_tweets.detect_country(tweet)
            {'name': 'Ukraine', 'code': 'UA',
             'coordinates': (31.178926, 49.352042)}
        """
        match = self.COUNTRY_NAME_RE.search(tweet['text'])
        if match:
            country = match.group('country')
            return {
                'name': self.COUNTRY_CODE_TO_NAME.get(country, country),
                'code': self.COUNTRY_NAME_TO_CODE.get(country, country),
                'coordinates': self.COORDINATES[country]
            }

    def detect_country_places(self, tweet):
        """Tweet might contain location information in place attribute

        https://dev.twitter.com/overview/api/tweets
        """
        if 'place' in tweet:
            code = tweet['place']['country_code']
            return {
                'name': self.COUNTRY_CODE_TO_NAME.get(code, ''),
                'code': code,
                'coordinates': self.COORDINATES[code],
            }

    def detect_country_user_location(self, tweet):
        """Detect tweet location based on user location"""
        if 'user' in tweet and 'location' in tweet['user']:
            code = tweet['user']['location'].split(' ')[-1]
            if code in self.COORDINATES:
                return {
                    'name': self.COUNTRY_CODE_TO_NAME.get(code, ''),
                    'code': code,
                    'coordinates': self.COORDINATES[code],
                }

    def detect_country(self, tweet):
        methods = [
            self.detect_country_places,
            self.detect_country_text_parse,
            self.detect_country_user_location,
        ]
        for method in methods:
            country_data = method(tweet)
            if country_data:
                return country_data

    def get(self, request):
        """Returns 10 latest tweet from MaplecroftRisk feed"""
        latest_tweets = self.twitter_api.GetUserTimeline(
            screen_name='MaplecroftRisk',
            count=10
        )

        ret_val = []
        for tweet in latest_tweets:
            t = tweet.AsDict()
            t['country'] = self.detect_country(t)
            ret_val.append(t)

        return JsonResponse(ret_val, safe=False)
