# -*- coding: utf-8 -*-

from django.test import TestCase
from ten_tweets.views import Latest10Tweets


class Latest10TweetsTestCase(TestCase):

    TWEETS = [
        {
            'text': (u'#Saudi likely to see rise in civil unrest - '
                     u'@MaplecroftRisk predictive data via @business '
                     u'Pessimist’s Guide to 2017… https://t.co/eaaExF5DFk'),
            'assert_country': None,
        },
        {
            'text': (u'Scenarios stress tested by @MaplecroftRisk '
                     u'https://t.co/nK8oKq8U3k'),
            'assert_country': None,
        },
        {
            'text': (u'Bloomberg Pessimist’s Guide to 2017 Egypt '
                     u'https://t.co/MJPvw6rebw … @MaplecroftRisk civil unrest '
                     u'predictions via @bbgvisualdata'),
            'assert_country': {
                'code': 'EG',
                'coordinates': (26.541764, 29.59237),
                'name': 'Egypt'
            },
        },
        {
            'text': (u'RT @Risk_Relations: Bloomberg Pessimist’s Guide to '
                     u'2017 Germany  https://t.co/X2D1967Hz6 via @business '
                     u'stress tested by @MaplecroftRisk'),
            'assert_country': {
                'code': 'DE',
                'coordinates': (51.072772, 10.56446),
                'name': 'Germany'
            },
        },
        {
            'text': (u'RT @johnfraher: Must read -- The Pessimist\'s Guide to '
                     u'2017 Russia https://t.co/qvtaWUMIzq @bpolitics '
                     u'https://t.co/q2VXJhIWlL'),
            'assert_country': {
                'code': 'RU',
                'coordinates': (63.884189, 103.660629),
                'name': u'Russia',
            },
        },
        {
            'text': (u'Bloomberg Pessimist’s Guide to 2017 '
                     u'https://t.co/MJPvw6rebw Featuring @MaplecroftRisk\'s '
                     u'predictive models on civil unrest via @bbgvisualdata'),
            'assert_country': None},
        {
            'text': (u'RT @jameslockharts: Our @chagelund @MaplecroftRisk '
                     u'take on HMG\'s tussle with the courts over Article'
                     u' 50... https://t.co/OSVT4hiwRZ'),
            'assert_country': None
        },
    ]

    def setUp(self):
        super(Latest10TweetsTestCase, self).setUp()
        self.view = Latest10Tweets()

    def test_detect_country(self):
        for tweet in self.TWEETS:
            country = self.view.detect_country_text_parse(tweet)
            assert country == tweet['assert_country']

    def test_detect_country_place(self):
        tweet = {
            'place': {
                'country': 'United States',
                'country_code': 'US',
            },
        }
        assert self.view.detect_country_places(tweet) == {
            'code': 'US',
            'coordinates': (39.818923, -102.372422),
            'name': 'USA'
        }

    def test_detect_country_user_location(self):
        tweet = {
            "user": {
                "location": "Bath, UK",
                "time_zone": "London",
            }
        }
        assert self.view.detect_country_user_location(tweet) == {
            'code': 'UK',
            'coordinates': (53.142645, -1.449462),
            'name': 'United Kingdom'
        }
