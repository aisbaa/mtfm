from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^10$', views.Latest10Tweets.as_view(), name='latest_10_tweets'),
]
