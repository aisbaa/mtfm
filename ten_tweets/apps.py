from django.apps import AppConfig


class TenTweetsConfig(AppConfig):
    name = 'ten_tweets'
