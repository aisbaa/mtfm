# Maplecroft Twitter Feed Map

Displays 10 latest tweets from Maplecroft on world map.

## Prerequisites ##

  - tox https://tox.readthedocs.io/en/latest/install.html
  - bower https://bower.io/#install-bower
  - gnu make (optional), usually comes with Linux OS

## Setup ##

    > tox            # this will setup and test python part of application
    > bower install  # this will install JavaScript libraries

Or alternatively run:

    > make setup

## Run ##

    > source .tox/py35/bin/activate
    > ./manage runserver

Or alternatively run:

    > make run
