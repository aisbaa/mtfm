from setuptools import setup, find_packages

setup(
    name='maplecroft-twitter-feed-map',
    packages=find_packages(exclude=['.tox']),
    install_requires=[
        'django >= 1.10.0',
        'python-twitter == 3.2',
        'six',
    ],
)
